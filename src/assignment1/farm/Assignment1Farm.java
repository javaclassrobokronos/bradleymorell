package assignment1.farm;

public class Assignment1Farm {
public Assignment1Farm() {
	example1.BarnYard by;
	by = new example1.BarnYard();
	example1.Chicken ch1;
	example1.Chicken ch2;
	example1.Chicken ch3;
	ch1 = new example1.Chicken();
	ch2 = new example1.Chicken();
	ch3 = new example1.Chicken();
	by.addChicken(ch1);
	by.addChicken(ch2);
	by.addChicken(ch3);
	ch1.start();
	ch2.start();
	example1.Pig pg1;
	example1.Pig pg2;
	pg1 = new example1.Pig();
	pg2 = new example1.Pig(); 
	by.addPig(pg1);
	by.addPig(pg2);
	pg1.start();
	pg2.start();
	example1.Butterfly bt;
	bt =  new example1.Butterfly();
	by.addButterfly(bt);
	
	
}
}
