package assignment1.farm;

public class Assignment1Zoo {
public Assignment1Zoo() {
	example1.BarnYard by1;
	example1.BarnYard by2;
	example1.BarnYard by3;
	by1 = new example1.BarnYard();
	by2 = new example1.BarnYard();
	by3 = new example1.BarnYard();
	example1.Chicken ch1;
	example1.Chicken ch2;
	ch1 = new example1.Chicken();
	ch2 = new example1.Chicken();
	by1.addChicken(ch1);
	by1.addChicken(ch2);
	ch1.start();
	example1.Butterfly bt1;
	example1.Butterfly bt2;
	example1.Butterfly bt3;
	bt1 = new example1.Butterfly();
	bt2 = new example1.Butterfly();
	bt3 = new example1.Butterfly();
	by2.addButterfly(bt1);
	by2.addButterfly(bt2);
	by2.addButterfly(bt3);
	bt1.start();
	bt2.start();
	bt3.start();
	example1.Pig pg1;
	example1.Pig pg2;
	pg1 = new example1.Pig();
	pg2 = new example1.Pig();
	by3.addPig(pg1);
	by3.addPig(pg2);
	pg1.start();
	
}
}
