package assignment2;

public class Farm {
 private example1.BarnYard _by;
 private example1.Chicken _ch;
 private example1.Pig _pg;
 private example1.Butterfly _bt;
 private example1.Chicken _ch2;
 public Farm() {
	 _by = new example1.BarnYard();
	 initialize();
	 start();
	 addBarnyard();
	 
 }
 public void initialize(){
	 _ch = new example1.Chicken();
	 _pg = new example1.Pig();
	 _bt = new example1.Butterfly();
	 _ch2 = new example1.Chicken();
	 
 }
  public void start(){
	  _ch.start();
	  _pg.start();
	  _bt.start();
	  _ch2.start();
  }
  public void addBarnyard(){
	  _by.addChicken(_ch);
	  _by.addChicken(_ch2);
	  _by.addPig(_pg);
	  _by.addButterfly(_bt);
  }
}
