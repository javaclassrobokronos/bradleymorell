package assignment2;

public class Zoo {
	private example1.BarnYard _by;
	private example1.BarnYard _by2;
	private example1.BarnYard _by3;
	public Zoo(){
		_by = new example1.BarnYard();
		_by2 = new example1.BarnYard();
		_by3 = new example1.BarnYard();
		addby();
		addby2();
		addby3();
		
	}
	public void addby(){
		example1.Chicken ch1;
		example1.Chicken ch2;
		ch1 = new example1.Chicken();
		ch2 = new example1.Chicken();
		_by.addChicken(ch1);
		_by.addChicken(ch2);
		ch2.start();
	}
	public void addby2(){
		example1.Pig pg;
		example1.Butterfly bt;
		pg = new example1.Pig();
		bt = new example1.Butterfly();
		_by2.addButterfly(bt);
		_by2.addPig(pg);
		bt.start();
		pg.start();
		addby();
		
	}
	public void addby3(){
		example1.Butterfly bt2;
		example1.Pig pg2;
		example1.Chicken ch3;
		ch3 = new example1.Chicken();
		bt2 = new example1.Butterfly();
		pg2 = new example1.Pig();
		_by3.addButterfly(bt2);
		_by3.addPig(pg2);
		_by3.addChicken(ch3);
		pg2.start();
		bt2.start();
	}

}
